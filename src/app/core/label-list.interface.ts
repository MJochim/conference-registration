export interface LabelList {
	headline?: string;
	submit?: string;
	abort?: string;
	back?: string;
	submitQuestion?: string;
	errorInvalidForm?: string;
	errorDuringSubmission?: string;
	registrationSuccessful?: string;
}
