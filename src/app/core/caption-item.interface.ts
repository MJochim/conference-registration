export interface CaptionItem {
	type: 'Caption';
	key: string;
	title?: string;
	text?: string;
}
