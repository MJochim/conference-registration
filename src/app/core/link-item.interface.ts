export interface LinkItem {
	type: 'Link';
	key: string;
	href: string;
	label?: string;
}
