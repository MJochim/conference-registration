# conference-registration

This is a webapp that lets visitors register for your conference. It is 
hosted on your premises to make sure that your vistors’ data is in your hands
 only.

Junge Sprachwissenschaft e. V. also offers to host the registration site for
you, which may be more appropriate than resorting to commercial hosting
providers.

The webapp was developed by Markus Johim for the association Junge 
Sprachwissenschaft e. V. It is licensed under the terms of the MIT License.

<https://junge-sprachwissenschaft.de/>

<https://anmeldung.stuts.de/> - The registration form in production use
